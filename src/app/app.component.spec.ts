import {TestBed, async, inject} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient, HttpParams} from '@angular/common/http';


describe('AppComponent', () => {
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [
        AppComponent
      ],
    });
    httpMock = TestBed.get(HttpTestingController);

  }));

  it('should work', inject([HttpClient], (http: HttpClient) => {
    const geocodeResponse = {
      'location': {
        'entity_type': 'subzone',
        'entity_id': 97456,
        'title': 'Rzeszów',
        'latitude': '50.0499990000',
        'longitude': '22.0000000000',
        'city_id': 267,
        'city_name': 'Rzeszów',
        'country_id': 163,
        'country_name': 'Poland'
      }
    };

    const params = new HttpParams()
      .append('lat', '50.04132')
      .append('lon', '21.99901');

    http
      .get('https://developers', {params}).subscribe();

    const req = httpMock
      .expectOne(`https://developers?lat=50.04132&lng=21.99901`);
    expect(req.request.method).toEqual('GET');

    req.flush(geocodeResponse);

  }));

  afterEach( () => {
    httpMock.verify();
  });
});
